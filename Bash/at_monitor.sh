#!/usr/bin/sh

# See how many at jobs are stacked up in the spool direcotry
NUMJOBS=`ls /var/spool/cron/atjobs | wc -l`
if (( NUMJOBS < 10 ))
then
    # There were less than 10, so assume all is good and just exit.
    exit 0
fi
# There were more than 10, so stop cron; kill any cron processes
# that are still hanging out there; and restart cron.
# Before we stop it, make sure it is actually running
NUMCRON=`ps -ef | grep -v grep | grep -c '/usr/sbin/cron'`
if (( NUMCRON > 0 ))
then
    /sbin/init.d/cron stop
    sleep 15
fi
# Get a list of any cron process still active
for i in `ps -ef | grep -v awk | awk '/cron/{print $2}'`
do
    echo "Killing $i"
    kill -9 $i
done
/sbin/init.d/cron start

THISHOST=`/usr/bin/uname -n`
/usr/bin/mailx -s "Restarted Cron on $THISHOST" ISOInf.Compute@jmsmucker.com << !
Cron was restarted on $THISHOST by the jms_at_monitor.sh script
because the number of entries in the /var/spool/cron/atjobs was
more than 10.
!